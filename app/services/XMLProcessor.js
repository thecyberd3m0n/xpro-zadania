/**
 * Created by tcd on 23.05.16.
 */
function XMLProcessor() {
    var xml;
    var valid = true;

    this.setXml = function (input) {
        try {
            xml = $.parseXML(input);
            valid = true;
        } catch (ex) {
            valid = false;
            return ex;
        }
    };

    this.getJSON = function () {
        if (xml) {
            var arr = [];
            $(xml).children().children().each(function (index,value) {
                arr.push({
                    "element": $(value)[0].nodeName,
                    "childrenCount": $(value).children().length
                });
            });
            return arr;

        } else {
            return null;
        }
    };

    this.isValid = function () {
        return valid;
    };

    return this;
}