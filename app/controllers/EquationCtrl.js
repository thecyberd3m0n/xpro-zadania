/**
 * Created by tcd on 23.05.16.
 */
function EquationCtrl($scope) {
    $scope.result = "";
    $scope.submit = function () {
        if ($scope.form.$valid) {
            var a = parseInt($scope.a);
            var b = parseInt($scope.b);
            if (a === 0 && b === 0) {
                $scope.result = "Identity equation";
            } else if (a === 0 && b !== 0) {
                $scope.result = "Conflict equation";
            } else {
                $scope.result = -b / a;
            }
        }
    }
}