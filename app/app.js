/**
 * Created by tcd on 23.05.16.
 */
angular.module('xpro', [ "ngMaterial"])
    .factory("XMLProcessor", XMLProcessor)
    .controller("EquationCtrl", EquationCtrl)
    .controller("XMLParseCtrl", XMLParseCtrl);