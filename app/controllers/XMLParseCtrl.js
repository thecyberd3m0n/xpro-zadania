/**
 * Created by tcd on 23.05.16.
 */
function XMLParseCtrl($scope, XMLProcessor) {
    $scope.xmlInput = "";
    $scope.json = "";
    $scope.invalidxml = false;

    $scope.$watch('xmlInput', processXml);

    function processXml(newVal) {
        if (!newVal) return;
        XMLProcessor.setXml(newVal);
        $scope.invalidxml = !XMLProcessor.isValid();
        if (!$scope.invalidxml) {
            $scope.json = XMLProcessor.getJSON();
        }
    }
}